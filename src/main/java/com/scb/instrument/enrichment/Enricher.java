package com.scb.instrument.enrichment;

import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.Instrument;

import java.util.List;

public interface Enricher<T extends Instrument> {

    List<FinalInstrumentData> publishFinalInstrumentData(List<T> instrumentList);
}
