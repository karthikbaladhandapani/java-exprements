package com.scb.instrument.enrichment;

import com.scb.instrument.enrichment.business.rule.LmeBusinessRule;
import com.scb.instrument.enrichment.business.rule.Rule;
import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.LmeBusinessModel;

import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

public class LmeInstrumentEnrichment implements Enricher<InstrumentData> {

    private Rule<LmeBusinessModel> lmeBusinessRule;

    public LmeInstrumentEnrichment(LmeBusinessRule lmeBusinessRule) {
        this.lmeBusinessRule = lmeBusinessRule;
    }

    private static final BiFunction<InstrumentData, LmeBusinessModel, FinalInstrumentData> TRANSFORM_FINAL_INSTRUMENT =
            (instrument, lmeBusinessModel) -> {
                String market = instrument.getMarket().split("_")[1];
               return new FinalInstrumentData(instrument.getInsCode(),null ,instrument.getLastTradingDate(),
                        instrument.getDeliveryDate(),market, instrument.getLabel(), lmeBusinessModel.getTradable());
            };

    @Override
    public List<FinalInstrumentData> publishFinalInstrumentData(List<InstrumentData> instrumentDataList) {
        return instrumentDataList.stream()
                .map(instrumentData -> TRANSFORM_FINAL_INSTRUMENT.apply(instrumentData, lmeBusinessRule.applyBusinessRule(instrumentData)))
                .collect(Collectors.toList());
    }
}
