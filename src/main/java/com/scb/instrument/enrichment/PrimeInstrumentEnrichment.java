package com.scb.instrument.enrichment;

import com.scb.instrument.enrichment.business.rule.PrimeBusinessRule;
import com.scb.instrument.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class PrimeInstrumentEnrichment implements Enricher<PrimeInstrumentData> {

    private PrimeBusinessRule primeBusinessRule;

    public PrimeInstrumentEnrichment(PrimeBusinessRule primeBusinessRule) {
        this.primeBusinessRule = primeBusinessRule;
    }

    private static final Predicate<InstrumentData> PREDICATE_PRIME = instrumentData -> instrumentData.getInstumentType() == Type.PRIME;


    private static final BiFunction<InstrumentData, PrimeBusinessRule, PrimeBusinessModel> GET_PRIME_BUSINESS_MODEL =
            ((instrumentData, primeBusinessRule1) -> primeBusinessRule1.applyBusinessRule(instrumentData));


    @Override
    public List<FinalInstrumentData> publishFinalInstrumentData(List<PrimeInstrumentData> instrumentList) {
        List<FinalInstrumentData> finalInstrumentDataList = new ArrayList<>();
        instrumentList.stream().filter(PREDICATE_PRIME).forEach(primeInstrumentData -> {
            Optional<PrimeBusinessModel> primeBusinessModelOptional = instrumentList.stream()
                    .map(instrumentData -> GET_PRIME_BUSINESS_MODEL.apply(instrumentData.getLmeInstrumentData(), primeBusinessRule)).findFirst();
            if (primeBusinessModelOptional.isPresent()) {
                PrimeBusinessModel primeBusinessModel = primeBusinessModelOptional.get();
                String market = primeInstrumentData.getMarket().split("_")[1];
                FinalInstrumentData finalInstrumentData = new FinalInstrumentData(primeInstrumentData.getInsCode(), primeInstrumentData.getExchangeCode(),
                        primeBusinessModel.getLastTradingDate(),primeBusinessModel.getDeliveryDate(), market, primeInstrumentData.getLabel(),
                        primeInstrumentData.getTradable());
                finalInstrumentDataList.add(finalInstrumentData);
            }
        });
        return finalInstrumentDataList;
    }
}
