package com.scb.instrument.enrichment.business.rule;

import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.LmeBusinessModel;

public class LmeBusinessRule implements Rule<LmeBusinessModel> {

    @Override
    public LmeBusinessModel applyBusinessRule(InstrumentData instrumentData) {
        return new LmeBusinessModel("TRUE");
    }
}
