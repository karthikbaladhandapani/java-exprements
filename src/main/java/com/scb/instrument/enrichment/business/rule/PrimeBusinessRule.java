package com.scb.instrument.enrichment.business.rule;

import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.PrimeBusinessModel;

public class PrimeBusinessRule implements Rule<PrimeBusinessModel> {

    @Override
    public PrimeBusinessModel applyBusinessRule(InstrumentData lmeInstrumentData) {
        return new PrimeBusinessModel(lmeInstrumentData.getLastTradingDate(),lmeInstrumentData.getDeliveryDate());
    }
}
