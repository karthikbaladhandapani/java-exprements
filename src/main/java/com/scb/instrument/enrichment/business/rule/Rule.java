package com.scb.instrument.enrichment.business.rule;

import com.scb.instrument.model.BusinessModel;
import com.scb.instrument.model.InstrumentData;

public interface Rule<T extends BusinessModel> {

    T applyBusinessRule(InstrumentData instrumentData);
}
