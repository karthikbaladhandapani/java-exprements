package com.scb.instrument.main;

import com.scb.instrument.command.InstrumentCommand;
import com.scb.instrument.command.LmeInstrumentCommand;
import com.scb.instrument.command.PrimeInstrumentCommand;
import com.scb.instrument.enrichment.LmeInstrumentEnrichment;
import com.scb.instrument.enrichment.PrimeInstrumentEnrichment;
import com.scb.instrument.enrichment.business.rule.LmeBusinessRule;
import com.scb.instrument.enrichment.business.rule.PrimeBusinessRule;
import com.scb.instrument.model.*;
import com.scb.instrument.util.InstrumentUtil;
import com.scb.instrument.util.MapperUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    private static void printInputDetails() {
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("                                        Instrument Enrichment                               ");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("Input XML Format        ");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("LME Format:");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("<instrumentData>\n" +
                "    <insCode>PB_03-2018</insCode>\n" +
                "    <lastTradingDate>15-03-2018</lastTradingDate>\n" +
                "    <deliveryDate>18-03-2018</deliveryDate>\n" +
                "    <market>LME_PB</market>\n" +
                "    <label>Label</label>\n" +
                "    <instumentType>LME</instumentType>\n" +
                "</instrumentData>");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("Prime Format:");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("<InstrumentData>\n" +
                "    <insCode>PRIME-PB_03-2018</insCode>\n" +
                "    <lastTradingDate>15-03-2018</lastTradingDate>\n" +
                "    <deliveryDate>18-03-2018</deliveryDate>\n" +
                "    <market>LME_PB</market>\n" +
                "    <label>Label</label>\n" +
                "    <instumentType>PRIME</instumentType>\n" +
                "    <tradable>TRUE</tradable>\n" +
                "    <exchangeCode>PB_03-2018</exchangeCode>\n" +
                "</InstrumentData>");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("Output XML Format        ");
        System.out.println("---------------------------------------------------------------------------------------------------");
        System.out.println("<InstrumentRefData>\n" +
                "    <lastTradingDate>15-03-2018</lastTradingDate>\n" +
                "    <deliveryDate>18-03-2018</deliveryDate>\n" +
                "    <market>PB</market>\n" +
                "    <label>Label</label>\n" +
                "    <tradable>TRUE</tradable>\n" +
                "</InstrumentRefData>");

    }

    public static void main(String[] args) {
        printInputDetails();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the Input Directory with Valid input XMLs ->");
        String inputPath = scanner.nextLine();

        System.out.println("Enter the Output Directory with Results XMLs ->");
        String resultPath = scanner.nextLine();

        List<? extends Instrument> instrumentDataList = null;

        try {
            instrumentDataList = Files.list(Paths.get(inputPath)).map(InstrumentUtil.PATH_TO_INPUT).collect(Collectors.toList());
            List<FinalInstrumentData> finalInstrumentDataList = new ArrayList<>();

            List<InstrumentData> insList = (List<InstrumentData>) instrumentDataList;

            MapperUtil.populateLMEInstrument(insList);
            Map<Type, List<InstrumentData>> typeListMap = insList.stream().collect(Collectors.groupingBy(instrumentData -> (instrumentData).getInstumentType(), Collectors.toList()));
            typeListMap.entrySet().stream().forEach(typeListEntry -> {
                finalInstrumentDataList.addAll(instrumentEnrichmentCommandMap().get(typeListEntry.getKey()).commandEnrich(typeListEntry.getValue()));
            });

//            finalInstrumentDataList.stream().forEach(System.out::println);
            aggregatedReport(finalInstrumentDataList);

            populateGeneratedFinalInstrumentedFile(finalInstrumentDataList, resultPath);

        } catch (IOException ioException) {
            System.out.println(ioException.getLocalizedMessage());
        }
//        System.out.println(instrumentDataList.size());
    }

    private static void populateGeneratedFinalInstrumentedFile(List<FinalInstrumentData> finalInstrumentDataList, String path) {
        finalInstrumentDataList.forEach(finalInstrumentData -> {
            String exchangeInsCode = finalInstrumentData.getExchangeInsCode();
            String fileName = new StringBuilder(finalInstrumentData.getInsCode()).append("-").append(exchangeInsCode != null ? exchangeInsCode : "").append(".xml").toString();
            String filePath = new StringJoiner("/").add(path).add(fileName).toString();
            InstrumentUtil.INSTRUMENT_DATA_PATH_FUNCTION.apply(finalInstrumentData, filePath);
        });
    }

    private static void aggregatedReport(List<FinalInstrumentData> finalInstrumentDataList) {
        Map<AggregationKey, List<FinalInstrumentData>> aggregationKeyListMap = finalInstrumentDataList.stream().
                collect(Collectors.groupingBy(InstrumentUtil.DATA_AGGREGATION_KEY_FUNCTION, Collectors.toList()));

        System.out.println("Exchange Code       Count");
        aggregationKeyListMap.entrySet().forEach(aggregationKeyListEntry -> {
            AggregationKey aggregationKey = aggregationKeyListEntry.getKey();
            if (aggregationKey.getExchangeCode() != null)
                System.out.println(aggregationKey.getExchangeCode() + "    " + aggregationKeyListEntry.getValue().size());
        });

    }


    private static Map<Type, InstrumentCommand> instrumentEnrichmentCommandMap() {
        Map<Type, InstrumentCommand> instrumentCommandMap = new HashMap<>();
        instrumentCommandMap.put(Type.LME, new LmeInstrumentCommand(new LmeInstrumentEnrichment(new LmeBusinessRule())));
        instrumentCommandMap.put(Type.PRIME, new PrimeInstrumentCommand(new PrimeInstrumentEnrichment(new PrimeBusinessRule())));
        return instrumentCommandMap;
    }
}
