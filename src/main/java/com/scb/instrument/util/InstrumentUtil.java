package com.scb.instrument.util;

import com.scb.instrument.model.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BiFunction;
import java.util.function.Function;

public class InstrumentUtil {

    public final static Function<FinalInstrumentData, AggregationKey> DATA_AGGREGATION_KEY_FUNCTION =
            finalInstrumentData -> new AggregationKey(finalInstrumentData.getExchangeInsCode());

    public final static Function<Path, ? extends Instrument> PATH_TO_INPUT =
            path -> {
                Instrument instrument = null;
                try {
                    JAXBContext jaxbContext = JAXBContext.newInstance(InstrumentData.class, PrimeInstrumentData.class);
                    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
                    instrument = (Instrument) unmarshaller.unmarshal(path.toFile());
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
                return instrument;
            };

    public static final BiFunction<FinalInstrumentData, String, Path> INSTRUMENT_DATA_PATH_FUNCTION = (finalInstrumentData, filePath) -> {
        try {
            Path path = Paths.get(filePath);
            JAXBContext jaxbContext = JAXBContext.newInstance(FinalInstrumentData.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(finalInstrumentData, path.toFile());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    };
}
