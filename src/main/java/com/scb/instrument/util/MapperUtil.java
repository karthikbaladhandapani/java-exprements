package com.scb.instrument.util;

import com.scb.instrument.mapper.PrimeMapKey;
import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.PrimeInstrumentData;
import com.scb.instrument.model.PrimeMapEntity;
import com.scb.instrument.model.Type;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MapperUtil {

    private final static Predicate<InstrumentData> PREDICATE_PRIME = instrumentData -> instrumentData.getInstumentType() == Type.PRIME;

    public static void populateLMEInstrument(List<InstrumentData> instruments) {
        PrimeMapKey primeMapKey = new PrimeMapKey();
        Map<String, List<InstrumentData>> instrumentListMap = instruments.stream().collect(Collectors.groupingBy(instrumentData -> instrumentData.getInsCode(), Collectors.toList()));

        instruments.forEach(instrumentData -> {
            if (PREDICATE_PRIME.test(instrumentData)) {
                PrimeInstrumentData primeInstrumentData = (PrimeInstrumentData) instrumentData;
                PrimeMapEntity primeMapEntity = primeMapKey.determineMapKey(primeInstrumentData);
                ((PrimeInstrumentData) instrumentData).setLmeInstrumentData(instrumentListMap.get(primeMapEntity.getExchageCode()).get(0));
            }
        });
    }
}
