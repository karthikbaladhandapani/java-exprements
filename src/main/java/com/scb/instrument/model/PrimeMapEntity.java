package com.scb.instrument.model;

public class PrimeMapEntity implements MapEntity {
    private String market;
    private String exchageCode;

    public PrimeMapEntity(String market, String exchageCode) {
        this.market = market;
        this.exchageCode = exchageCode;
    }

    public String getMarket() {
        return market;
    }

    public String getExchageCode() {
        return exchageCode;
    }
}
