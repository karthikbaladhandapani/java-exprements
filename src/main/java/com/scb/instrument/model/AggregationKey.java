package com.scb.instrument.model;

public class AggregationKey {


    private final String exchangeCode;

    public AggregationKey(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AggregationKey that = (AggregationKey) o;

        return exchangeCode != null ? exchangeCode.equals(that.exchangeCode) : that.exchangeCode == null;
    }

    @Override
    public int hashCode() {
        return exchangeCode != null ? exchangeCode.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "AggregationKey{" +
                "exchangeCode='" + exchangeCode + '\'' +
                '}';
    }
}
