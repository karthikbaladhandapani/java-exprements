package com.scb.instrument.model;

public class LmeBusinessModel implements BusinessModel {

    private String tradable;

    public LmeBusinessModel(String tradable) {
        this.tradable = tradable;
    }

    public String getTradable() {
        return tradable;
    }

    public void setTradable(String tradable) {
        this.tradable = tradable;
    }
}
