package com.scb.instrument.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "InstrumentData")
public class PrimeInstrumentData extends InstrumentData {

    @XmlElement
    private String tradable;

    @XmlElement
    private String exchangeCode;

    private InstrumentData lmeInstrumentData;

    public PrimeInstrumentData(Builder builder) {
        super(builder);
        this.tradable = builder.tradable;
        this.exchangeCode = builder.exchangeCode;
    }

    private PrimeInstrumentData() {

    }

    public PrimeInstrumentData(InstrumentData.Builder builder) {
        super(builder);
    }

    public InstrumentData getLmeInstrumentData() {
        return lmeInstrumentData;
    }

    public void setLmeInstrumentData(InstrumentData lmeInstrumentData) {
        this.lmeInstrumentData = lmeInstrumentData;
    }

    public String getTradable() {
        return tradable;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }



    public static class Builder extends InstrumentData.Builder {
        private String tradable;
        private String exchangeCode;

        public Builder tradable(String tradable) {
            this.tradable = tradable;
            return this;
        }

        public Builder exchangeCode(String exchangeCode) {
            this.exchangeCode = exchangeCode;
            return this;
        }

        public PrimeInstrumentData build() {
            return new PrimeInstrumentData(this);
        }
    }
}
