package com.scb.instrument.model;

import com.scb.instrument.reader.DateAdaptor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlRootElement
public class InstrumentData implements Instrument {

    @XmlElement
    private String insCode;

    @XmlElement(name = "lastTradingDate")
    @XmlJavaTypeAdapter(DateAdaptor.class)
    private Date lastTradingDate;

    @XmlElement(name = "deliveryDate")
    @XmlJavaTypeAdapter(DateAdaptor.class)
    private Date deliveryDate;

    @XmlElement
    private String market;

    @XmlElement
    private String label;

    @XmlElement
    private Type instumentType;

    /*@XmlElement(required = false)
    private String tradable;

    @XmlElement(required = false)
    private String exchangeCode;*/

    public InstrumentData(Builder builder) {
        this.insCode = builder.insCode;
        this.lastTradingDate = builder.lastTradingDate;
        this.deliveryDate = builder.deliveryDate;
        this.market = builder.market;
        this.label = builder.label;
        this.instumentType = builder.instrumentType;
//        this.tradable = builder.tradable;
//        this.exchangeCode = builder.exchangeCode;
    }

    protected InstrumentData() {
    }

    public String getInsCode() {
        return insCode;
    }

    public Date getLastTradingDate() {
        return lastTradingDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public String getLabel() {
        return label;
    }

    public Type getInstumentType() {
        return instumentType;
    }

    /*public String getTradable() {
        return tradable;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }*/

    public static class Builder {
        private String insCode;
        private Date lastTradingDate;
        private Date deliveryDate;
        private String market;
        private String label;
        private Type instrumentType;
//        private String tradable;
//        private String exchangeCode;

        public Builder insCode(String insCode) {
            this.insCode = insCode;
            return this;
        }

        public Builder lastTradingDate(Date lastTradingDate) {
            this.lastTradingDate = lastTradingDate;
            return this;
        }

        public Builder deliveryDate(Date deliveryDate) {
            this.deliveryDate = deliveryDate;
            return this;
        }

        public Builder market(String market) {
            this.market = market;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder instrumentType(Type instrumentType) {
            this.instrumentType = instrumentType;
            return this;
        }

        /*public Builder exchangeCode(String exchangeCode) {
            this.exchangeCode = exchangeCode;
            return this;
        }


        public Builder tradable(String tradable) {
            this.tradable = tradable;
            return this;
        }*/

        public InstrumentData build() {
            return new InstrumentData(this);
        }
    }
}
