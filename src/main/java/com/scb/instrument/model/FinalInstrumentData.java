package com.scb.instrument.model;

import com.scb.instrument.reader.DateAdaptor;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@XmlRootElement(name = "InstrumentRefData")
public class FinalInstrumentData implements Instrument {

    private String insCode;
    private String exchangeInsCode;
    @XmlElement
    @XmlJavaTypeAdapter(DateAdaptor.class)
    private Date lastTradingDate;
    @XmlElement
    @XmlJavaTypeAdapter(DateAdaptor.class)
    private Date deliveryDate;
    @XmlElement
    private String market;
    @XmlElement
    private String label;
    @XmlElement
    private String tradable;

    public FinalInstrumentData() {
    }

    public FinalInstrumentData(String insCode, String exchangeInsCode, Date lastTradingDate, Date deliveryDate, String market, String label, String tradable) {
        this.insCode = insCode;
        this.exchangeInsCode = exchangeInsCode;
        this.lastTradingDate = lastTradingDate;
        this.deliveryDate = deliveryDate;
        this.market = market;
        this.label = label;
        this.tradable = tradable;
    }

    public Date getLastTradingDate() {
        return lastTradingDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public String getMarket() {
        return market;
    }

    public String getLabel() {
        return label;
    }

    public String getTradable() {
        return tradable;
    }

    public String getInsCode() {
        return insCode;
    }

    public String getExchangeInsCode() {
        return exchangeInsCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinalInstrumentData that = (FinalInstrumentData) o;

        if (insCode != null ? !insCode.equals(that.insCode) : that.insCode != null) return false;
        if (exchangeInsCode != null ? !exchangeInsCode.equals(that.exchangeInsCode) : that.exchangeInsCode != null)
            return false;
        if (lastTradingDate != null ? !lastTradingDate.equals(that.lastTradingDate) : that.lastTradingDate != null)
            return false;
        if (deliveryDate != null ? !deliveryDate.equals(that.deliveryDate) : that.deliveryDate != null) return false;
        if (market != null ? !market.equals(that.market) : that.market != null) return false;
        if (label != null ? !label.equals(that.label) : that.label != null) return false;
        return tradable != null ? tradable.equals(that.tradable) : that.tradable == null;
    }

    @Override
    public int hashCode() {
        int result = insCode != null ? insCode.hashCode() : 0;
        result = 31 * result + (exchangeInsCode != null ? exchangeInsCode.hashCode() : 0);
        result = 31 * result + (lastTradingDate != null ? lastTradingDate.hashCode() : 0);
        result = 31 * result + (deliveryDate != null ? deliveryDate.hashCode() : 0);
        result = 31 * result + (market != null ? market.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        result = 31 * result + (tradable != null ? tradable.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FinalInstrumentData{" +
                "insCode='" + insCode + '\'' +
                ", exchangeInsCode='" + exchangeInsCode + '\'' +
                ", lastTradingDate=" + lastTradingDate +
                ", deliveryDate=" + deliveryDate +
                ", market='" + market + '\'' +
                ", label='" + label + '\'' +
                ", tradable='" + tradable + '\'' +
                '}';
    }
}
