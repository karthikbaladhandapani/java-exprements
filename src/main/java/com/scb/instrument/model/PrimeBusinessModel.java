package com.scb.instrument.model;

import java.util.Date;

public class PrimeBusinessModel implements BusinessModel {

    private Date lastTradingDate;
    private Date deliveryDate;

    public PrimeBusinessModel(Date lastTradingDate, Date deliveryDate) {
        this.lastTradingDate = lastTradingDate;
        this.deliveryDate = deliveryDate;
    }

    public Date getLastTradingDate() {
        return lastTradingDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }
}
