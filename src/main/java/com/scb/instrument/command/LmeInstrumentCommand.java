package com.scb.instrument.command;

import com.scb.instrument.enrichment.Enricher;
import com.scb.instrument.enrichment.LmeInstrumentEnrichment;
import com.scb.instrument.enrichment.business.rule.LmeBusinessRule;
import com.scb.instrument.enrichment.business.rule.Rule;
import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.LmeBusinessModel;

import java.util.List;

public class LmeInstrumentCommand implements InstrumentCommand<InstrumentData, FinalInstrumentData> {

    private Enricher<InstrumentData> instrumentDataEnricher;

    public LmeInstrumentCommand(Enricher<InstrumentData> instrumentDataEnricher) {
        this.instrumentDataEnricher = instrumentDataEnricher;
    }

    @Override
    public List<FinalInstrumentData> commandEnrich(List<InstrumentData> instruments) {
        return instrumentDataEnricher.publishFinalInstrumentData(instruments);
    }
}
