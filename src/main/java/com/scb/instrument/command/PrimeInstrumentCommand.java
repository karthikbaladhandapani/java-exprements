package com.scb.instrument.command;

import com.scb.instrument.enrichment.Enricher;
import com.scb.instrument.enrichment.PrimeInstrumentEnrichment;
import com.scb.instrument.enrichment.business.rule.PrimeBusinessRule;
import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.PrimeInstrumentData;

import java.util.ArrayList;
import java.util.List;

public class PrimeInstrumentCommand implements InstrumentCommand<PrimeInstrumentData,FinalInstrumentData> {

    private Enricher<PrimeInstrumentData> primeInstrumentEnrichment;

    public PrimeInstrumentCommand(Enricher<PrimeInstrumentData> primeInstrumentEnrichment) {
        this.primeInstrumentEnrichment = primeInstrumentEnrichment;
    }

    @Override
    public List<FinalInstrumentData> commandEnrich(List<PrimeInstrumentData> instruments) {
        List<PrimeInstrumentData> primeInstrumentDataList = new ArrayList(instruments);
        return primeInstrumentEnrichment.publishFinalInstrumentData(primeInstrumentDataList);
    }
}
