package com.scb.instrument.command;

import com.scb.instrument.model.Instrument;

import java.util.List;

public interface InstrumentCommand<T extends Instrument, U >  {

    List<U> commandEnrich(List<T> instruments);
}
