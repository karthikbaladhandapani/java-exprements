package com.scb.instrument.mapper;

import com.scb.instrument.model.PrimeInstrumentData;
import com.scb.instrument.model.PrimeMapEntity;

public class PrimeMapKey implements MapKey<PrimeInstrumentData, PrimeMapEntity> {

    @Override
    public PrimeMapEntity determineMapKey(PrimeInstrumentData primeInstrumentData) {
        return new PrimeMapEntity(primeInstrumentData.getMarket(),primeInstrumentData.getExchangeCode());
    }
}
