package com.scb.instrument.mapper;

import com.scb.instrument.model.Instrument;
import com.scb.instrument.model.MapEntity;

public interface MapKey<T extends Instrument, U extends MapEntity> {

    U determineMapKey(T instrument);
}
