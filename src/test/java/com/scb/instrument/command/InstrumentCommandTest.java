package com.scb.instrument.command;

import com.scb.instrument.enrichment.Enricher;
import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.InstrumentData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import com.scb.instrument.util.TestInputUtil;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class InstrumentCommandTest {

    @Mock
    private Enricher<InstrumentData> instrumentDataEnricher;

    @InjectMocks
    private LmeInstrumentCommand lmeInstrumentCommand;

    @InjectMocks
    private PrimeInstrumentCommand primeInstrumentCommand;


    @Test
    public void testLmeInstrumentCommandTest() {
        Mockito.when(lmeInstrumentCommand.commandEnrich(Mockito.anyList())).
                thenReturn(TestInputUtil.finalInstrumentDataList());
        List<FinalInstrumentData> finalInstrumentData = lmeInstrumentCommand.commandEnrich(TestInputUtil.buildInstrumentDataList());
        Assert.assertNotNull(finalInstrumentData);
    }

    @Test
    public void testPrimeInstrumentCommandTest() {
        Mockito.when(primeInstrumentCommand.commandEnrich(Mockito.anyList())).
                thenReturn(TestInputUtil.finalInstrumentDataList());
        List<FinalInstrumentData> finalInstrumentData = primeInstrumentCommand
                .commandEnrich(TestInputUtil.buildPrimeInstrumentDataListWithValidLMETrade());
        Assert.assertNotNull(finalInstrumentData);

    }



}
