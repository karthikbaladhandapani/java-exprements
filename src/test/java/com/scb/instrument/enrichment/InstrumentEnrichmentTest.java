package com.scb.instrument.enrichment;

import com.scb.instrument.enrichment.business.rule.LmeBusinessRule;
import com.scb.instrument.enrichment.business.rule.PrimeBusinessRule;
import com.scb.instrument.model.FinalInstrumentData;
import org.junit.Assert;
import org.junit.Test;
import com.scb.instrument.util.TestInputUtil;

import java.util.List;

public class InstrumentEnrichmentTest {

    @Test
    public void testLmeInstrumentEnrichment() {
        LmeInstrumentEnrichment lmeInstrumentEnrichment = new LmeInstrumentEnrichment(new LmeBusinessRule());
        List<FinalInstrumentData> finalInstrumentDataList = lmeInstrumentEnrichment.publishFinalInstrumentData(TestInputUtil.buildInstrumentDataList());
        Assert.assertNotNull(finalInstrumentDataList);
        Assert.assertFalse(finalInstrumentDataList.isEmpty());
    }

    @Test
    public void testPrimeInstrumentEnrichmentWithNoLME() {
        PrimeInstrumentEnrichment primeInstrumentEnrichment= new PrimeInstrumentEnrichment(new PrimeBusinessRule());
        List<FinalInstrumentData> finalInstrumentDataList = primeInstrumentEnrichment
                .publishFinalInstrumentData(TestInputUtil.buildPrimeInstrumentDataListWithNoValidLMETrade());
        Assert.assertNotNull(finalInstrumentDataList);
        Assert.assertTrue(finalInstrumentDataList.isEmpty());
    }

    @Test
    public void testPrimeInstrumentEnrichmentWithValidData() {
        PrimeInstrumentEnrichment primeInstrumentEnrichment= new PrimeInstrumentEnrichment(new PrimeBusinessRule());
        List<FinalInstrumentData> finalInstrumentDataList = primeInstrumentEnrichment
                .publishFinalInstrumentData(TestInputUtil.buildPrimeInstrumentDataListWithValidLMETrade());

        Assert.assertNotNull(finalInstrumentDataList);
        Assert.assertFalse(finalInstrumentDataList.isEmpty());
    }
}
