package com.scb.instrument.parser;

import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.PrimeInstrumentData;
import com.scb.instrument.model.Type;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class InstrumentParser {

//    public InstrumentData parseInstrumentData() {

    @Test
    public void testParser() {
        Date lastTradingDate = new GregorianCalendar(2018,Calendar.MARCH,15).getTime();
        Date deliveryDate = new GregorianCalendar(2018,Calendar.MARCH,18).getTime();

        InstrumentData lmeInstrumentData = new InstrumentData.Builder().instrumentType(Type.LME).insCode("PB_03-2018").lastTradingDate(lastTradingDate)
            .deliveryDate(deliveryDate).market("LME_PB").label("AAA").build();

        PrimeInstrumentData primeInstrmentData = (PrimeInstrumentData) new PrimeInstrumentData.Builder().tradable("TRUE").exchangeCode("PB_03-2018").instrumentType(Type.PRIME).insCode("PRIME-PB_03-2018").lastTradingDate(lastTradingDate)
                .deliveryDate(deliveryDate).market("LME_PB").label("AAA").build();

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(InstrumentData.class,PrimeInstrumentData.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.marshal(lmeInstrumentData,new File("lme-instrument-1.xml"));
            marshaller.marshal(primeInstrmentData,new File("prime-instrument-1.xml"));

            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            InstrumentData lmeInstrumentDataUM = (InstrumentData) unmarshaller.unmarshal(new File("lme-instrument-1.xml"));
            InstrumentData primeInstrumentDataUM = (InstrumentData) unmarshaller.unmarshal(new File("prime-instrument-1.xml"));

            Assert.assertNotNull(lmeInstrumentDataUM);
            Assert.assertNotNull(primeInstrumentDataUM);


        } catch (JAXBException e) {
            e.printStackTrace();
        }

    }

}
