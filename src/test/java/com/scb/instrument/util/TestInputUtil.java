package com.scb.instrument.util;

import com.scb.instrument.model.FinalInstrumentData;
import com.scb.instrument.model.InstrumentData;
import com.scb.instrument.model.PrimeInstrumentData;
import com.scb.instrument.model.Type;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class TestInputUtil {

    public static List<InstrumentData> buildInstrumentDataList() {
        InstrumentData instrumentData1 = new InstrumentData.Builder().label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.LME)
                .insCode("PB_03-18").build();

        InstrumentData instrumentData2 = new InstrumentData.Builder().label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.LME)
                .insCode("PB_04-18").build();
        return Arrays.asList(instrumentData1, instrumentData2);
    }

    public static List<PrimeInstrumentData> buildPrimeInstrumentDataListWithNoValidLMETrade() {
        PrimeInstrumentData primeInstrumentData = (PrimeInstrumentData) new PrimeInstrumentData.Builder().exchangeCode("PB_03-18").tradable("TRUE").label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.LME)
                .insCode("PB_03-18").build();

        PrimeInstrumentData primeInstrumentData1 = (PrimeInstrumentData) new PrimeInstrumentData.Builder().exchangeCode("PB_03-18").tradable("TRUE").label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.LME)
                .insCode("PB_03-18").build();
        return Arrays.asList(primeInstrumentData, primeInstrumentData1);
    }

    public static List<PrimeInstrumentData> buildPrimeInstrumentDataListWithValidLMETrade() {
        InstrumentData instrumentData1 = new InstrumentData.Builder().label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.LME)
                .insCode("PB_03-18").build();

        PrimeInstrumentData primeInstrumentData1 = (PrimeInstrumentData) new PrimeInstrumentData.Builder().exchangeCode("PB_03-18").tradable("TRUE").label("Label")
                .market("LME_PB").deliveryDate(new Date()).lastTradingDate(new Date()).instrumentType(Type.PRIME)
                .insCode("PRIME-PB_03-18").build();
        primeInstrumentData1.setLmeInstrumentData(instrumentData1);
        return Arrays.asList(primeInstrumentData1);
    }

    public static List<FinalInstrumentData> finalInstrumentDataList() {
        return Arrays.asList(
                new FinalInstrumentData("PB-03-18", null, new Date(), new Date(), "PB", "Label", "True"),
                new FinalInstrumentData("PB-04-18", null, new Date(), new Date(), "PB", "Label", "True"));

    }
}
